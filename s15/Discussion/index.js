// Single-line comment

/* Multi-line comment
	Multi-line comment 
	Ctrl + Shift + / */

console.log("Hello World");

// Variables
/* 	- it is used to contain data
	
	Syntax:
		let/const variableName;

		let - keyword usually used in declaring a variable
		const - keyword usually used in declaring constant variable
*/

let myVariable;

console.log(myVariable);

// Initializing Variables - instance when a variable is given it's initial/ starting value.
/* Syntax:
	let/const varialbeName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let	productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

// Reassigning variable values
/*Syntax:
	variableName = newValue;
*/

productPrice = 'laptop';
console.log(productPrice);

// Declares a variable first
let supplier;

supplier	=	"John Smith Tradings";

// Multiple variable declaration
let productCode	= 'DC017', productBrand	=	'Dell';
console.log(productCode, productBrand);

// Data Types
/*	1. String - handles a series of characters*/
let	country		=	'Philippines';
let	province	=	'Metro Manila';

// Concatenating strings (+ symbol is used)
let	fullAddress	=	province+' '+country;
console.log(fullAddress);

let greeting	= "I live in the " + country;
console.log(greeting);

// Escape Character (\) in strings is combination with other characters can produce a different effect (\n)
let mailAddress	=	'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

// Numbers
/* Integers/ whole numbers */
let headCount	=	32;
console.log(headCount);

/* Decimal/ fractions */
let grade		= 98.7;
console.log(grade);

/* Exponential notation*/
let	planetDistance	=	2e10;
console.log(planetDistance);

/* Combining text/numbers and strings */
console.log("John's grade last quarter is: " + grade);

// Boolean - used to store values relating to the state of certain things
let	isMarried = true;
let inGoodConduct	= false;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Array - special kind of data type that stores multiple values
/* Syntax:
	let/const arrayName = [elementA, elementB, elementC, ...];
*/
let	grades	=	[98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Objects - special kinds of data type that mimics real world objects/items.
/* Syntax:
	let/const objectName = {
		propertyA : value,
		propertyB : value
	}
*/
let person = {
	fullName 	: 'Juan Dela Cruz',
	age 		: 35,
	isMarriaed 	: false,
	contact 	: ["0917 123  4567", "8123 4567"],
	address 	: {
		houseNumber : '345',
		city 		: 'Manila'
	}
};
console.log(person);

let myGrades	= {
	firstGrading	: 98.7,
	secondGrading	: 92.1,
	thirdGrading	: 90.2,
	fourthGrading	: 94.6
};
console.log(myGrades);

// typeof operator - used to determining the type of data or value of variable
console.log(typeof myGrades);
console.log(typeof grades);

// null - used to intentionally express the absence of a value in a variable declaration/initialization
let	spouse		=	null;
let	myNumber	=	0;
let myString	=	'';
console.log(spouse + myNumber + myString);

// undefined
let fullName;
console.log(fullName);