// [Section] Comparison Query Operators

// $gt/$gte (Greater than)
/*
	- Allows us to find documents that have field number values greater than or equal to a specified field

	Syntax: 
		db.collectionName.find({field: $gt : value});
*/

db.users.find({"age": {$gt: 50}});

// $lt/$lte (Less than)
/*
	- Allows us to find documents that have field number values less than or equal to a specified field

	Syntax:
		db.collectionName.find({field: $lt : value});
*/

db.users.find({"age": {$lt: 50}});

// $ne (Not equal)
/*
	- Allows us to find documents that have field number values not equal to specified value.

	Syntax: 
		db.collectionName.find({field: $ne : value});
*/

db.users.find({"age": {$ne: 82}});

// $in operator
/*
	- Allows us to find documents with specific match criteria one field using different values

	Syntax: 
		db.collectionName.find({field: $in : value});
*/

db.users.find({"lastName" : {$in : ['Hawking', 'Doe']}});
db.users.find({"course" : {$in : ['HTML','React']}});

db.users.find({"course" : {$nin : ['HTML','React']}});

// [Section] Logical Query Operations

// $or operator
/*
	- allows us to find documents that match a single criteria from multiple provided search criteria

	Syntax:
		db.collectionName.find({ $or: [{fieldA: valueA}, {fieldB : valueB}]});
*/

db.users.find({$or: [{"firstName" : "Neil"},{"age" : 25}]});
db.users.find({$or: [{"firstName" : "Neil"},{"age" : {$gt: 30}}]});

// $and operator
/*
	- allows us to find documents matching multiple criteria in a single field
*/

db.users.find({$and : [{"age": {$ne:82}},{"age": {$ne:76}}]});

// Field Projection
/*
	- retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response
	- when dealing with complex data structures, there might be instances when field are not useful for the query that we are trying to accomplish

	- To help with readability of the values returned, we can include/ exclude fields from the response
*/

// Inclusion
/*
	- allows us to include/ add specific fields only when retrieving documents
	- value provided is 1 to denote that the field is being excluded

	Syntax:
		db.users.find({criteria}, {field: 1});
*/
db.users.find(
	{	
		"firstName" :"Jane"
	},
	{
		"firstName" : 1,
		"lastName" : 1,
		"contact" : 1
	}
)

// Exlusion
/*
	- allows us to exclude/ remove specific fields only when retrieving documents
	- value provided is 0 to denote that the field is being excluded

	Syntax:
		db.users.find({criteria}, {field: 0});
*/

db.users.find(
	{	
		"firstName" :"Jane"
	},
	{
		"contact" : 0,
		"department" : 0,
		"_id" : 0
	}
)

// Suppressing ID Field
db.users.find(
	{	
		"firstName" :"Jane"
	},
	{
		"firstName" : 1,
		"lastName" : 1,
		"contact" : 1,
		"_id" : 0
	}
)

// Returning specific fields in an embedded document
db.users.find(
	{	
		"firstName": "Jane"
	},
	{
		"firstName" : 1,
		"lastName" : 1,
		"contact.phone" : 1
	}
)

// Suppressing specific fields in embedded documents
db.users.find(
	{	
		"firstName" : "Jane"
	},
	{
		"contact.phone": 0
	}
);

// Project specific array elements in returned array
// The $slice operator allows us to retrieve only 1 element that matches the search criteria
db.users.insertOne({
    namearr: [
        {
            fname: "juan"

        },
        {
            fname: "juan"
        }
    ]
});

db.users.find(
	{ "namearr": 
		{ 
			"namea": "juan" 
		} 
	}, 
	{ "namearr": 
		{ $slice: 1 } 
	}
)

// [Section] Evaluation Query Operators

// $regex
/*
	- allows us to find documents that match a specific string pattern using regular expressions
	- case sensitive

	Syntax:
		db.users.find({field: {$regex: 'pattern'}, $options: "optionValue"})
*/

// Case sensitive query
db.users.find({"firstName" : {$regex: 'N'}});
db.users.find({"firstName" : {$regex: 'n'}});
db.users.find({"firstName" : {$regex: 'Stev'}});

// Case insensitive query
db.users.find(
	{"firstName":
		{
			$regex: 'j',
			$options: 'i'
		}
	}
);

