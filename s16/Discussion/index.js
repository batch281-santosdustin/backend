/* JavaScript Operators */

// Arithmetic Operators

let	x	= 1397;
let y 	= 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let	diff = x - y;
console.log("Result of subtraction operator: " + diff);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let modulo = y % x;
console.log("Result of remainder: " + modulo);

// Assignment Operators (=)
let assignmentNumber = 8;

// Addition assignment operator (+=)
assignmentNumber = assignmentNumber + 2;

console.log("Result of addition assignment: " + assignmentNumber);

// Shorthand
assignmentNumber += 2;
console.log("Result of addition assignment number: " + assignmentNumber);

// Subtraction/ Multiplication/ Division Assignment Operator (-=, *=, /=)
assignmentNumber -= 2;
console.log("Subtraction assignment: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Multiplication assignment: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Division assignmnet: " + assignmentNumber);

// Multiple Operators and Parentheses
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Multiple operation: " + mdas);

// Parentheses
let	pemdas = 1 + (2 - 3) * (4 / 5);
console.log("PEMDAS: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("New PEMDAS: " + pemdas);

// Increment and Decrement
let z = 1;
let	increment = ++z;
console.log("Pre-increment: " + z);
console.log("Pre-increment: " + increment);

increment = z++;
console.log("Post-increment: " + z);
console.log("Post-increment: " + increment);

let decrement = --z;
console.log("Pre-decrement: " + decrement);
console.log("Pre-decrement: " + z);

decrement = z--;
console.log("Post-decrement: " + decrement);
console.log("Post-decrement: " + z);

// Type Coercion
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = true + 1;
console.log(numC);
console.log(typeof numC);

// Comparison Operators
let juan = 'juan';

// Equality Operator (==)
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log(1 == true);
console.log('juan' == juan);

// Inequality Operator (!=)
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log(1 != true);
console.log('juan' != juan);

// Strict Equality Operator (===) Should follow type
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log(1 === true);
console.log('juan' === juan);

// Strictly Inequality Operator (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log(1 !== true);
console.log('juan' !== juan);

// Relational Operators
let a = 50;
let b = 65;

// Greater than (>)
let isGreaterThan = a > b;
console.log(isGreaterThan);

let isLessThan = a < b;
console.log(isLessThan);

let isGTorEqual = a >= b;
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical And Operator (&&)
let andRequirementsMet = isLegalAge && isRegistered;
console.log(andRequirementsMet);

// Logical Or Operator (||)
let orRequirementsMet = isLegalAge || isRegistered;
console.log(orRequirementsMet);

// Logical Not Operator (!)
let notLegalAge = !isLegalAge;
let notRegistered = !isRegistered;
console.log(notLegalAge);
console.log(notRegistered);