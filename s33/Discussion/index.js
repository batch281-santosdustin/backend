// [SECTION] JavaScript Synchronous vs Asynchronous
/*
	JavaScript is by default synchronous meaning that only one statement is executed at a time.

	Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background
*/

// Creating a simple fetch request
// [SECTION] Getting all posts
/*
	The fetch API allows you to asynchronously request for a resource (data).

	A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Check the status of the request
/*
	Retrieve all posts following the REST API (retrieve, /posts, GET)

	By using the then method we can now check for the status of the promise
*/
fetch('https://jsonplaceholder.typicode.com/posts')
	// The "fetch" method will return a "promise" that resolves to a "response" object
	// The "then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected"
	.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts')
	// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in you collection
	.then((response) => response.json())
	// Print the converted JSON value from the "fetch" request
	// Using multiple "then" methods creates a promise chain
	.then((json) => console.log(json));

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for

// Creates asynchronous function

async function fetchData(){
// waits for the "fetch" method to complete then stores the value in the "result" variable

	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	// Result returned by fetch returns a promise
	console.log(result);
	// The returned "response" is an object
	console.log(typeof result);
	// We can not access the content of the "response" by directly accessing its body's property
	console.log(result.body);

	// Converts data from the "Response" object as JSON
	let json = await result.json();
	// Print out the content of the "Response object"
	console.log(json);
}

fetchData();

// Process a GET request using postman
/*
	ON POSTMAN:
		URL: https://jsonplaceholder.typicode.com/posts
		METHOD: GET
*/

// [SECTION] Getting a specific post
// Retrieves a specific post following the REST API (Retrieve, /posts/:id, GET)
fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then((response) => response.json())
	.then((json) => console.log(json));

// [SECTION] Creating a post

// Creates a new post following the REST API (create, /posts/:id, POST)
fetch('https://jsonplaceholder.typicode.com/posts', {
	// Sets the method of the "Request" object to "POST" following the REST API
	// Default method is GET
	method: 'POST',

	// Sets the method of the "Request" object to "POST" following REST API
	// Specified that the content will be in a JSON structure
	headers: {
		'Content-type' : 'application/json',
	},
	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello, world!',
		userID: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Updating a post
// Updates a specific post following the REST API (update, /posts/:id, PUT)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type' : 'application/json',
	},
	body: JSON.stringify({
		title: 'Updated post',
		body: 'Hello again',
		userID: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Updating a post using PATCH
// Updates a specific post following the REST APO (update, /posts/:id, Patch)
// The difference between PUT and PATCH is the number of properties being changed

// PUT is used to update the whole project
// PATCH is used to update a single/ several properties

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type' : 'application/json',
	},
	body: JSON.stringify({
		title: 'Corrected post',
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Deleting a post
// Deleting a specific post following the REST API
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});

// [SECTION] Filtering posts

// The data can be filtered by sending the usedId along with the url
// Information sent via the url can be done by adding the question mark symbol(?)
fetch('https://jsonplaceholder.typicode.com/posts?userID=1')
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Retrieving nested/ related comments to posts
// Retrieving comments for a specific post following the REST API (retrieve, /posts/:id/comments)
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));