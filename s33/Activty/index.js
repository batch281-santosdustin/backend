// console.log("Hellow");

//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){
    return await (

       //add fetch here.       
       fetch('https://jsonplaceholder.typicode.com/todos')
       .then((response) => response.json())
       .then((json) => json)     
   );
}



// Getting all to do list item
async function getAllToDo(){
   return await (

      //add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos')
      .then((response) => response.json())
      // .then((json) => {
      //    let titles = json.map((item) => item.title);
      //    return titles;
      // })
      .then((json) => titles = json.map((item) => item.title))
  ); 
}

getAllToDo().then((titles) => {
   console.log(titles);
});

// [Section] Creating a to do list item using POST method
async function createToDo(todoItem){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos', {
         method: 'POST',
         headers: {
            'Content-Type' : 'application/json',
         },
         body: JSON.stringify(todoItem)
      })

      .then((response) => response.json())
      .then((json) => json)
   );
}

const newToDo = {
   completed : false,
   title : "Created To Do List Item",
   userId : 1
};

createToDo(newToDo).then((titles) => {
   console.log(titles);
});

// [Section] Updating a to do list item using PUT method
async function updateToDo(newToDo){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/1', {
         method: 'PUT',
         headers: {
            'Content-type' : 'application/json',
         },
         body: JSON.stringify(newToDo)
      })

      .then((response) => response.json())
      .then((json) => json)
   );
}

const updateToDoItem = {
   dateCompleted  : "Pending",
   description    : "To update the my-to-do-list with a different data structure",
   status         : "Pending",
   title          : "Updated To-Do-List Item",
   userId         : 1
};

updateToDo(updateToDoItem).then((titles) => {
   console.log(titles);
});

// [Section] Deleting a to do list item
async function deleteToDo(toBeDeleted){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/${toBeDeleted}', {
         method: 'DELETE'
      })

      .then((response) => response.json())
      .then((json) => json)
   );

}

const toDelete = 1;

deleteToDo(toDelete).then((titles) => {
   console.log(titles);
});

//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}


