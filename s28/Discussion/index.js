CRUD Operations

// Create

db.users.insertOne({
	"firstName" : "Jane",
	"lastName" : "Doe",
	"age" : 21,
	"contact" : {
		"phone" : "85479123",
		"email" : "janedoe@mail.com"
	},
	"course": ["CSS", "JavaScript", "Python"],
	"department" : "none"
});

// Insert many collections
db.users.insertMany([
	{
		"firstName" : "Steven",
		"lastName" : "Hawking",
		"age" : 76,
		"contact" : {
			"phone" : "85479123",
			"email" : "stevenhawking@mail.com"
		},
		"course": ["React", "PHP", "Python"],
		"department" : "none"
	},

	{
		"firstName" : "Neil",
		"lastName" : "Armstrong",
		"age" : 82,
		"contact" : {
			"phone" : "85479123",
			"email" : "neilarmstrong@mail.com"
		},
		"course": ["React", "Laravel", "Sass"],
		"department" : "none"
	}
]);

// Finding Documents (Read)
// Find Operator

// Retrieving all documents
db.users.find();

// Retrieving single/ one document
db.users.find({ "firstName" : "Steven"});
// or
db.users.find({ "_id" : ObjectId("64661703f0686c6f8a9d17c1")});

// Retrieving with multiple parameters
db.users.find({ "lastName": "Armstrong", "age" : 82});

// Updating a single document to update
db.users.insertOne({
	"firstName" : "Test",
	"lastName" : "Test",
	"age" : 0,
	"contact" : {
		"phone" : "00000000",
		"email" : "test@mail.com"
	},
	"course": [],
	"department" : "none"
});

db.users.updateOne(
	{"firstName" : "Test"},
	{
		$set : {
			"firstName" : "Bill",
			"lastName" : "Gates",
			"age" : 65,
			"contact" : {
				"phone" : "8764321",
				"email" : "billgates@mail.com"
			},
			"course": ["PHP", "Laravel", "HTML"],
			"department" : "Operations",
			"status" : "active"
		}
	}
);

// Updating multiple documents
db.users.updateMany(
	{"departmnent" : "none"},
	{
		$set : {"department" : "HR"}
	}
);

// Replace one
db.users.replaceOne(
	{"firstName" : "Bill"},
	{
		"firstName" : "Bill",
		"lastName" : "Gates",
		"age" : 65,
		"contact" : {
			"phone" : "8764321",
			"email" : "billgates@mail.com"
		},
		"course": ["PHP", "Laravel", "HTML"],
		"department" : "Operations"
	}
);

// Creating a document to delete
db.users.insertOne({
	"firstName" : "test"
});

db.users.deleteOne({
	"firstName" : "test"
});

// Query an embedded document
db.users.find({
	"contact" : {
			"phone" : "8764321",
			"email" : "billgates@mail.com"
	}
});