console.log("Eouwphouxsz");

// While Loop
/*
	- evaluates condition, if returned true, it will execute statements

	Syntax:
		while (expression/ condition) {
			statement;
		}
*/

let count = 5;

while (count > 0) {
	console.log("While: " + count);
	count--;
}

let count_activity = 1;

while (count_activity <= 5) {
	console.log("While: " + count_activity);
	count_activity++;
}

// Do-while Loop
/*
	- iterates statements within a number of times based on a condition.

	Syntax:
		do {
			statement;
		} while (expression/condition);
*/

let number = parseInt(prompt("Give a number: "));

do {
	console.log("Do while: " + number);

	number += 1;
} while (number < 10);

let even = 2;

do {
	console.log(even);
	even += 2;
} while (even <= 10);

// For Loop
/*
	- looping construct that is more flexible than other loops. 
		- Parts: Initialization, Condition, Final expression/ step expression

	Syntax:
		for (initialization; expression/ condition; finalExpression) {
			statement;
		}
*/

for (let count = 0; count <= 20; count++) {
	console.log(count);
}

let myString = "alex";

console.log(myString.length);

// Accessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

let myName = "ecnAlubMa";

for (let i = 0; i < myName.length; i++) {
	if (
		myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u'
	) {
		console.log(3);
	} else {
		console.log(myName[i]);
	}
}

Continue and Break Statements

for (let count = 0; count <= 20; count++) {
	if(count % 2 === 0) {
		continue;
	}

	console.log("Continue and Break: " + count);

	if(count > 10) {
		break;
	}
}

let name = "alexandro";

for (let i = 0; i < name.length; i++) {
	console.log(name[i]);

	if(name[i].toLowerCase() === "a") {
		console.log("Continue to next.");
		continue;
	}

	if(name[i].toLowerCase() === "d") {
		console.log("Break");
		break;
	}
}