// console.log("Eouwphouxsz");

// Array Traversal

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

let mixedArr = [12, 'Dustin', null, undefined];

let tasks = [
		'drink',
		'eat',
		'inhale',
		'sleep'
	];

let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Mumbai';

let cities = [city1, city2, city3];

console.log(tasks);
console.log(cities);

// Array Length Property
console.log("Array length: ");
console.log(tasks.length);
console.log(cities.length);

let fullName = "Aizaac Isaac";
console.log(fullName.length);

tasks.length = tasks.length - 1;
console.log(tasks);

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);
theBeatles[theBeatles.length - 1] = "Dustin";
console.log(theBeatles);

// Accessing elements
console.log(grades[1]);
console.log(computerBrands[3]);
console.log(grades[20]);

// Re-assigning an element
grades[0] = 99;
console.log(grades);

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker = lakersLegends[2];
console.log("Accessing arrays using variables: " + currentLaker);

// Accessing last element of an array
let bullsLegends = ["jordan", "pippen", "rodman", "rose", "kukoc"];

let lastElementIndex = bullsLegends.length - 1;
console.log(bullsLegends[lastElementIndex]);

// Directy access the expression
console.log(bullsLegends[bullsLegends.length - 2]);

// Adding itmes into the array
let newArr = [];
for (i = 0; i < 4; i++) {
	newArr[i] = prompt("Enter FF character: ");
}
console.log(newArr);

// let newArr = [];
// newArr[0] = "Cloud Strife";

let numArr = [5, 12, 30, 46, 40];

for (i = 0; i < numArr.length; i++) {
	if (numArr[i] % 5 === 0) {
		console.log(numArr[i] + " is divisible by 5");
	} else {
		console.log(numArr[i] + " is not divisible by 5");
	}
}

// Multidimensional Array
/*
	2x2 two dimensional array
	let twoDim = [[elem1, elem2], [elem3, elem4]]
					0 		1 		0 		1
						0 				1
	twoDim[0][0];
*/

let twoDim = [['Kenzo', 'Marie'], ['Ally', 'Mimay']];

for (i = 0; i < 2; i++) {
	for (j = 0; j < 2; j++) {
		console.log(twoDim[i][j]);
	}
}