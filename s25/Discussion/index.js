// console.log("Hello");

// JSON as objects
/*{
	"city" : "Quezon city",
	"province" : "Metro Manila",
	"country" : "Philippines";
}*/

// JSON as array
/*"cities": [
	{"city" : "Quezon City", "province" : "Metro Manila", "country" : "Philippines"},
	{"city" : "Manila City", "province" : "Metro Manila", "country" : "Philippines"},
	{"city" : "Makati City", "province" : "Metro Manila", "country" : "Philippines"}
]*/

// Mini activity - create a JSON array that will hold three breeds of dogs with properties.
/*"dogs" : [
	{"name" : "Ally", "age" : "4", "breed" : "shihtzu"},
	{"name" : "Mimay", "age" : "3", "breed" : "shihtzu"},
	{"name" : "Barkley", "age" : "7", "breed" : "labrador"}
]*/

// JSON Methods
// Convert data into stringified JSON

let batchesArray = [{ batchName: 'Batch X'}, { batchName: 'Batch Y'}];

// the "stringify" method is used to convert JavaScript objects into a string
console.log('Result from stringify: ');
console.log(JSON.stringify(batchesArray));

// Alternative in which we can stringify an object directly

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data);

// Using stringify method with variables
// User details
/*let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('What is your city?'),
	country: prompt('Which country does your city address belong to?')
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName : lastName,
	age: age,
	address: address
});

console.log(otherData);*/

// ACTIVITY: Create a JSON data that will accept user car details with variables brand, type, year.

/*let carBrand = prompt('What is the car brand? ');
let carType = prompt('What is the car type?');
let carYear = prompt('What is the car year?');

let carData = JSON.stringify({
	Brand: carBrand,
	Type: carType,
	Year: carYear
});

console.log(carData);
*/
// Converting Stringified JSON to Javascript objects

let batchesJSON = '[{ "batchName": "Batch X"}, { "batchName": "Batch Y"}]';

console.log('Result from parse method: ');
console.log(JSON.parse(batchesJSON));