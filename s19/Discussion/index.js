// console.log("Hello, World");

// Selection Control Structures
	/*
		- it sorts out whether the statement/s are to be executed based on the condition whether it is true or false.
		- two-way (true or false)
		- multi-way selection
	*/

// If-Else Statement
/*	if(condition) {
		// statement
	} else if {
		// statement
	}
*/
let numA = -1;

if(numA < 0) {
	console.log("The number is less than 0");
}

console.log(numA < 0);

let	city = "Alabama";

if (city === "New York") {
	console.log("Welcome to " + city);
} else {
	console.log("You are in " + city);
}

let numB = 1;

if (numA > 0) {
	console.log("Hellow");
} else if (numB > 0) {
	console.log("Mundo");
}

city = "Manila";

if (city === "New York") {
	console.log("Welcome to " + city);
} else if (city === "Tokyo") {
	console.log("You are in " + city);
} else {
	console.log("You are not in New York nor in Tokyo");
}

let age = parseInt(prompt("Enter your age: "));

if (age <= 18) {
	console.log("You are underage");
} else {
	console.log("You can drink");
}

/*
	Mini Activty
		-create a function that will receive any value of height as an argument when you invoke it.
		-then create conditional statements:
			- if height is less than or equal to 150, print "Did not pass the min height requirement." in the console.
			- but if the height is greater than 150, print "Passed the min height requirement." in the console
*/

let height = parseInt(prompt("Enter height: "));

if (height <= 150) {
	console.log("Did not pass the min height requirement.");
} else {
	console.log("Passed the min height requirement.");
}

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30) {
		return "Not a typhoon";
	} else if (windSpeed <= 61) {
		return "Tropical depression detected";
	} else if (windSpeed >= 62 && windSpeed <= 88) {
		return "Tropical storm detected";
	} else if (windSpeed >= 89 && windSpeed <= 177) {
		return "Severe tropical storm detected";
	} else {
		return "Typhoon detected";
	}
}

message = determineTyphoonIntensity(parseInt(prompt("Enter typhoon intensity: ")));
console.log(message);

// Truthy and Falsy

/*
	In JS a truthy value is a value that is considered true when encountered in a boolean context.

	Falsy value:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/	

// Truthy Examples:

let word = "true"
if(word){
	console.log("Truthy");
};

if(true) {
	console.log("Truthy");
};

if(1) {
	console.log("Truthy");
};


// Falsy Examples:
if(false) {
	console.log("Falsy");
};

if(0) {
	console.log("Falsy");
};

if(undefined) {
	console.log("Falsy");
};

if(null) {
	console.log("Falsy");
};

if(-0) {
	console.log("Falsy");
};

if(NaN) {
	console.log("Falsy");
};

// Condition Ternary Operation - for short codes

/*
	Ternary operator takes in three operands:
		1. Condition
		2. Expression to execute if the condition is true/ truthy.
		3. Expression to execute if the condition is false/ falsy.

	Syntax:
		(condition) ? ifTrue_Expression : isFalse_Expression.
*/

let ternaryResult = (1 < 18) ? "Condition is True" : "Condition is False";

console.log("Result: " + ternaryResult);

// Multiple Statement Execution

let name;

function isOfLegalAge() {
	name = "John";
	return "You are of the legal age limit";
}

function isUnderAge() {
	name = "Jane";
	return "You are under the age limit";
}

let yourAge = parseInt(prompt("Enter your age: "));
console.log(yourAge);

let legalAge = (yourAge > 18) ? isOfLegalAge() : isUnderAge();
console.log(legalAge);

// Switch Statement
/*
	Can be used as an alternative to if ... else statements where the data to be used in the condition is of an expected 
	input.

	Syntax:
		switch (expression/ condition) {
			case <value>:
				statement;
				break;
			default:
				statement;
				break;
		}
*/

let day = prompt("What day of the week is today?").toLowerCase();

console.log(day);

switch (day) {
	case 'monday':
		console.log("Red");
		break;
	case 'tuesday':
		console.log("Blue");
		break;
	case 'wednesday':
		console.log("Green");
		break;
	case 'thursday':
		console.log("Orange");
		break;
	case 'friday':
		console.log("Pink");
		break;
	case 'saturday':
		console.log("Violet");
		break; 
	case 'sunday':
		console.log("Yellow");
		break;
	default:
		console.log("Wrong input");
		break;
}

// Try-Catch-Finally Statement
/*
	- try catch is commonly used for error handling
	- will still function even if the statement is not complete/ without the finally code block.
*/

function showIntensityAlert (windSpeed) {
	try {
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch (error) {
		console.log(typeof error);
		console.log(error);
		console.warn(error.message);
	}
	finally {
		alert("Intensity updates will show new alert!");
	}
}

showIntensityAlert(parseInt(prompt("Enter wind speed: ")));

let error = 0;
console.log(error);