let http = require("http");

// Mock database
let directory = [
	{ 
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{ 
		"name" : "Jobert",
		"email" : "jobert@mail.com"
	}
];

let port = 4000;

let app = http.createServer(function (request, response) {
	// Route for returning all items upon receiving a GET request
	if(request.url == "/users" && request.method == "GET") {
		// Sets status code 200 and response output to JSON data type
		response.writeHead(200, {'Content-Type' : 'application/json'});

		response.write(JSON.stringify(directory));
		response.end();
	}

	// Route for creating a new data upon receiving a POST request
	if(request.url == "/users" && request.method == "POST") {
		// "requestBody" acts as a placeholder for the resource/ data to be created later on
		let requestBody = '';

		// Stream is a sequence of data
		request.on('data', function(data) {
			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		})

		request.on('end', function(){
			console.log(typeof requestBody);

			// Converts the string requestBody to JSON 
			requestBody = JSON.parse(requestBody);

			// Creates a new object representing the new mock database
			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			}

			// Add the new user to the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type' : 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	}
});

app.listen(port, () => console.log(`Server running at localhost: ${port}`));

// 