let http = require("http");
let port = 4000;

let app = http.createServer(function (request, response) {

	// If the url is http://localhost:4000/, send a response Welcome to Booking System
	if (request.url == '/' && request.method == "GET" ) {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Welcome to the booking system");
	}

	// If the url is http://localhost:4000/profile, send a response Welcome to your profile!
	if (request.url == '/profile' && request.method == "GET" ) {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Welcome to your profile!");
	}

	// If the url is http://localhost:4000/courses, send a response Here’s our courses available
	if (request.url == '/courses' && request.method == "GET" ) {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Here’s our courses available");
	}

	// If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
	if (request.url == '/addcourse' && request.method == "POST" ) {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Add a course to our resources");
	}

	// If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
	if ((request.url == '/updatecourse' || request.url == '/updateCourse') && request.method == "PUT" ) {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Update a course to our resources");
	}

	// If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
	if ((request.url == '/archivecourse' || request.url == '/archiveCourse') && request.method == "DELETE" ) {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Archive courses to our resources");
	}
});

//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;