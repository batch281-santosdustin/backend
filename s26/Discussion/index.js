// console.log("Hello");

// What is a Database?
/*
	an organized collection of information or data
	Data is raw and does not carry any specific meaning. 
	Information is a group of organized data that contains logical meaning.

	Databases typically refer to information stored in a computer system but it can also refer to physical databases

	CRUD Operations
		create - insert
		read - select
		update
		delete

	Relational database
		a type of database where data is stored as a set of tables with rows and columns

	For unstructure data (data that can't fit into a strict tabular format)
		NoSQL databases are commonly used

*/

// What is SQL?
/*
	SQL stands for structured query language

	Information represented in SQL are done in the form of tables having columns and rows that provide information about the data


	SQL databases require tables and information provided in it's columnds to be defined before they are  created and then used to store information.
	This ensures that information stored in SQL databases to be of the same data structure preventing any errors of storing data that are incomplete.

	Becauseof SQL databases require tables and columns to be defined before creation and use, NoSQL databases are becoming more popular due to their flexibility of being able to change the data structure of information in databases on the fly.
	Developers can omit the process of recreating tables and backing up data and re importing them as needed on certain occasions.
*/

// What is NoSQL?
/*
	NoSQL means Not only SQL. It was conceptualized when capturing complex, unstructured data become more difficult
*/

// What is MongoDB?
/*
	MongoDB is an open-source database and the leading NoSQL database.

	Its language is highly expressive, and generally friendly to those already familiar with the JSON structure.

	Mongo in MongoDB is a part of the word HUMONGOUS which then means huge or enormous.
*/

// What does data stored in MongoDB look like?
/*
	Main advantage of this data structure is that it is stored in MongoDB using the JSON formot

	A developer does not need to keep in mind the difference bet writing code for the program itself and data access

	Also, in MongoDB:
		- tables = collections
		- rows = documents
		- columns = fields
*/