//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Exponent Operator
/*
	let getCube = 8 ** 3;
	console.log(getCube);
*/
let getCube = Math.pow(8, 3);

// // Template Literals
console.log(`The cube of 8 is ${getCube}.`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

const [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street} ${state} ${zipCode}.`);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const {name, species, weight, measurement} = animal;

console.log(`${name} was a ${species}. He weighed ${weight} with a mesurement of ${measurement}`);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(number);
})

const initialValue = 0;
const sum = numbers.reduce(
		(accumulator, currentValue) => accumulator + currentValue, initialValue
	);
console.log(sum);
// // Javascript Classes
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog('Ally', '4', 'Shihtzu');
console.log(myDog);




//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}