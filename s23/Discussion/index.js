// console.log("Hello");

// Objects
/*
	data type that is used to represent real world objects

	Syntax:
		let objectName = {
			propertyA: valueA,
			propertyB: valueB
		}
*/

let cellphone = {
	name: 'Nokia 3210',
	manufacturerDate: 1999
}

console.log('Result from creating objects');
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
/*
	Syntax:
		function objectName(propertyA, propertyB) {
			this.propertyA = propertyA,
			this.propertyB = propertyB
		}
*/

function Laptop(name, manufacturerDate) {
	this.name = name;
	this.manufacturerDate = manufacturerDate;
}

let laptop = new Laptop('Lenovo', 2008);
console.log('Result from creating objects using object constructors');
console.log(laptop);

let myLaptop = new Laptop('Toshiba', 2012);
console.log('Result from creating objects using object constructors');
console.log(myLaptop);

console.log('Result from dot notation: ' + myLaptop.name);
console.log('Result from square bracket notation: ' + myLaptop['name']);

// Accessing array objects
let array = [laptop, myLaptop];

// Accessing object properties in array using square bracket notation
console.log(array[0]['name']);

// Accessing object properties in array using dot notation
console.log(array[0].name);

// Initializing/ Adding/ Deleting/ Reassigning object properties

let car = {};

// Initializing/ Adding object properties using dot notation
car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation: ');
console.log(car);

// Initializing/ Adding object properties using square bracket notation
car['manufacturer date'] = 2019;
console.log(car['manufacturer date']);
console.log(car['Manufacturer Date']);
console.log(car.manufacturerDate);
console.log('Result from adding properties using square bracket notation');
console.log(car);

// Deleting object properties
delete car['manufacturer date'];
console.log('Result from deleting properties');
console.log(car);

// Reassinging values to object properties
car.name = 'Dodge Charger R/T';
console.log('Result of reassigning');
console.log(car);

// Object methods

let person = {
	name: 'John',
	talk: function() {
		console.log("Hello my name is " + this.name);
	}
}

console.log(person);
console.log('Result from object methods: ');
person.talk();

// Adding methods to objects
person.walk = function () {
	console.log(this.name + ' walked 25 steps forward.');
}

person.walk();

// Real world application of objects

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This Pokemon tacked targetPokemon");
		console.log("targetPokemon's health is now reducted to _targetPokemonhealth_")
	},
	faint: function() {
		console.log("Pokemon fainted");
	}
}

console.log(myPokemon);

// Creating an object constructor
function Pokemon(name, level) {
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	}
	this.faint = function(target) {
		console.log(this.name + ' fainted.');
	}
}

// Create new instance of the 'Pokemon' object
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
rattata.tackle(pikachu);