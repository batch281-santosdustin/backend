// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
// Initialize/add the given object properties and methods
// Properties
// Methods
// Check if all properties and methods were properly added

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Psyduck", "Kyogre", "Rayquaza", "Groudon"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(pokemon) {
		console.log("Pikachu! I choose you!");
	}
}

console.log(trainer);

// Access object properties using dot notation
console.log("Result of dot notation: ");
console.log(trainer.name);

// Access object properties using square bracket notation
console.log("Result of square bracket notation: ")
console.log(trainer['pokemon']);

// Access the trainer "talk" method
console.log("Result of talk method: ");
trainer.talk();

// Create a constructor function called Pokemon for creating a pokemon
let myPokemon = {
	name: "Psyduck",
	level: 1,
	health: 12,
	attack: 21,
	tackle: function() {
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reducted to _targetPokemonhealth_");
	},
	faint: function() {
		console.log("Pokemon fainted");
	}
}

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = Math.round(7.1 * level);
	this.attack = Math.round(3.4 * level);

	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		target.health = target.health - this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);

		if(target.health <= 0) {
			console.log(target.name + " fainted!");
		}
	}
}

// Create/instantiate a new pokemon
let kyogre = new Pokemon("Kyogre", 35);

// Create/instantiate a new pokemon
let rayquaza = new Pokemon("Rayquaza", 41);

// Create/instantiate a new pokemon
let groudon = new Pokemon("Groudon", 1);

console.log(kyogre);
console.log(rayquaza);
console.log(groudon);

// Invoke the tackle method and target a different object
kyogre.tackle(rayquaza);
console.log(rayquaza);
kyogre.tackle(rayquaza);
console.log(rayquaza);

// Invoke the tackle method and target a different object
rayquaza.tackle(kyogre);
console.log(kyogre);

rayquaza.tackle(groudon);
console.log(groudon);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}