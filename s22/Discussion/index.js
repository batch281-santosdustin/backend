//console.log("Eouwh");

// Mutator Methods


// Non-mutator Methods
/*
	- functions that do not modify or change an array after they're created
	- do not manipulate the original array performing various tasks such as returning elements
*/

// Iteration Methods
/*
	- loops designed to perform repetitive tasks on arrays
*/

// Mutator Methods:

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log("Current array: ");
console.log(fruits);

// push
/*
	adds an element in the ned of an array and returns the array's length

	Syntax:
		arrayName.push();
*/

fruits.push("Mango");

console.log("Mutated array from push method.");
console.log(fruits);

// Adding multiple
fruits.push("Avocado", "Guava");
console.log(fruits);

// pop()
/*
	removes the last element

	Syntax: arrayName.pop();
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

// Unshift
/*
	adds one or more elements at the beginning of an array

	Syntax:
		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB');
*/

fruits.unshift('Lime', 'Banana');
console.log("Mutated array from unshift method.");
console.log(fruits);

// Shift
/*
	removes an element at the beginning of an array and returns the removed element

	Syntax:
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from unshift method.");
console.log(fruits);

// Splice
/*
	simultaneously removes elements from a specified index and adds elements

	Syntax:
		arrayName.splice(startingIndex, deleteCount, 'newElementA',... 'newElementZ');
*/

fruits.splice(2, 3, "Calamansi", "Lime", "Cherry");
console.log("Splice method");
console.log(fruits);

// Sort
/*
	rearranges the array elements in alphanumeric order

	Syntax:
		arrayName.sort();
*/

fruits.sort();
console.log(fruits);

// Reverse sort
/*
	Syntax"
		arrayName.reverse();
*/

fruits.reverse();
console.log(fruits);

// Non-mutator Method
/*
	functions that do not modify that do not change an array after they are created
	these methods do not manipulate
*/

let countries = ["US", "PH", "CAN", "SG", "PH", "FR", "GE", "CH", "KR"];
console.log(countries);

// Index of
/*
	returns the index number of the first matching element found in an array.
	if no match, result will be -1
	the search process will be done from first element proceeding to the last element.

	Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log("indexOf: " + firstIndex);

firstIndex = countries.indexOf('UAE');
console.log("indexOf: " + firstIndex);

// last index of
/*
	finding a element starting from the last element
	returns index number of the last matchin element found in an array
	search process will be done from last element proceeding to first element

	Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/

let lastIndex = countries.lastIndexOf('PH');
console.log('lastIndexOf: ' + lastIndex);

lastIndex = countries.lastIndexOf('PH', 4);
console.log('lastIndexOf: ' + lastIndex);

lastIndex = countries.lastIndexOf('PH', 3);
console.log('lastIndexOf: ' + lastIndex);

// Slice
/*
	portion/ slices elements from an element and returns a new array

	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

let slicedArray = countries.slice(2);
console.log("Slice method: ");
console.log(slicedArray);

let slicedArray2 = countries.slice(2, 4);
console.log("Slice method: ");
console.log(slicedArray2);

let slicedArray3 = countries.slice(-3);
console.log("Slice method: ");
console.log(slicedArray3);

// toString
/*
	returns an array as string separated by commas

	Syntax:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log(stringArray);

// concat
/*
	combines 2 arrays and returns the combined result

	Syntax:
		arrayNameA.concat(arrayNameB);
*/

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log("concat method: ");
console.log(tasks);

let tasks2 = tasksArrayA.concat("smell express", "throw react");
console.log(tasks2);

// join
/*
	returns an array as string separated by specified separator string
*/

let users = ['John', 'Juan', 'Jay'];
console.log("result of join: ");
console.log(users.join(', '));

// Iteration Methods
/*
	Iteration methods are loops designed to perform repetitive tasks on arrays
	Iteration methods loops over all items in an array.
	Useful for manipulating array data resulting in complex tasks
	Array iteration methods normally work with a function supplied as an argument
	How these function works is by performing tasks that are pre-defined within an array's method.
*/

// forEach()
/*
        - Similar to a for loop that iterates on each array element.
        - For each item in the array, the anonymous function passed in the forEach() method will be run.
        - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
        - Variable names for arrays are normally written in the plural form of the data stored in an array
		- It's common practice to use the singular form of the array content for parameter names used in array loops
        - forEach() does not return anything.

        - Syntax
            arrayName.forEach(function(indivElement) {
                statement
            })
*/

tasks2.forEach(function(per_task) {
	console.log(per_task);
})

filteredTasks = [];
tasks2.forEach(function(tasks){
	if (tasks.length > 10){
		filteredTasks.push(tasks);
	}
})

console.log(filteredTasks);

// map()
/* 
    Iterates on each element AND returns new array with different values depending on the result of the function's operation
    This is useful for performing tasks where mutating/changing the elements are required
    Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
    
    Syntax:
        let/const resultArray = arrayName.map(function(indivElement))
*/
let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number) {
	return number * number;
})

console.log(numberMap);

// Every
/*
	checks if all statement in an array meet the given condition
*/

let allValid = numbers.every(function(number){
	return number < 3;
})

console.log(allValid);

// Some
/*
	checks if some statement in an array meet the given condition
*/

let someValid = numbers.some(function(number){
	return number < 2;
})

console.log(someValid);

// Filter
/*
	returns new array that contains elements which meets the given condition
*/

let filterValid = numbers.filter(function(number){
	return (number < 2);
})

console.log(filterValid);

// Include
/*
	include method checks if the argument passed can be found in the array
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Ally");
console.log(productFound2);

// Chain Method
/*
        - Methods can be "chained" using them one after another
        - The result of the first method is used on the second method until all "chained" methods have been resolved
        - How chaining resolves in our example:
            1. The "product" element will be converted into all lowercase letters
            2. The resulting lowercased string is used in the "includes" method
*/
    let filteredProducts = products.filter(function(product){
        return product.toLowerCase().includes('a');
})

console.log(filteredProducts);

// reduce()
/*
	evaluates elements from left to right and returns the array into a single value

	- The "accumulator" parameter in the function stores the result for every iteration of the loop
	- The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
	- How the "reduce" method works
	   1. The first/result element in the array is stored in the "accumulator" parameter
	   2. The second/next element in the array is stored in the "currentValue" parameter
	   3. An operation is performed on the two elemen
	----
	 - How the "reduce" method works
	            1. The first/result element in the array is stored in the "accumulator" parameter
	            2. The second/next element in the array is stored in the "currentValue" parameter
	            3. An operation is performed on the two elements
	            4. The loop repeats step 1-3 until all elements have been worked on
	===================
	- The "accumulator" parameter in the function stores the result for every iteration of the loop
	        - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
	- How the "reduce" method works
	            1. The first/result element in the array is stored in the "accumulator" parameter
	            2. The second/next element in the array is stored in the "currentValue" parameter
	            3. An operation is performed on the two elements
	            4. The loop repeats step 1-3 until all elements have been worked on


	Syntax:
		let/const resultArray = arrayName.reduce(accumulator, currentValue) {
			return expression/ operation;
		}
*/

let iteration = 0;
let reducedArray = numbers.reduce(function(acc, cur){
	console.warn('current iteration: ' + ++iteration);
	console.log('accumulator: ' + acc);
	console.log('current value: ' + cur);

	return acc + cur;
})

console.log(reducedArray);

// reducing string arrays
    let list = ['Hello', 'Again', 'World'];

    let reducedJoin = list.reduce(function(x, y) {
        return x + ' ' + y;
    });
    console.log("Result of reduce method: " + reducedJoin);