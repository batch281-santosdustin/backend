console.log("Hello, test");

// These numbers are inputs to nos. 1-2
let genFirstNumber = 102;
let genSecondNumber = 24;

// This number is the input for radius of circle for no. 3
let radiusCircle = 10;
let piOfCircle = 3.14;

// These numbers are inputs for the calculation of average for no.4
let firstAve = 102;
let secondAve = 13;
let thirdAve = 205;
let fourthAve = 15;

// These numbers are the score and total score for no.5
let myScore = 15;
let totalScore = 50;
let passingGrade = .75;

/*	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function*/

function addNumber (firstNumber, secondNumber) {
	var sumNumber = firstNumber + secondNumber;
	console.log("Displayed sum of " + firstNumber + " and " + secondNumber);
	console.log(sumNumber);
}

addNumber(genFirstNumber, genSecondNumber);

function subtractNumber (firstNumber, secondNumber) {
	var diffNumber = firstNumber - secondNumber;
	console.log("Displayed sum of " + firstNumber + " and " + secondNumber);
	console.log(diffNumber);
}

subtractNumber(genFirstNumber, genSecondNumber);

/*	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.*/

function returnProduct(firstNumber, secondNumber) {
	return firstNumber * secondNumber;
	console.log("This message will not be printed.")
}

let multiplyNumber = returnProduct(genFirstNumber, genSecondNumber);
console.log("The product of " + genFirstNumber + " and " + genSecondNumber);
console.log(multiplyNumber); 

function returnQuotient(firstNumber, secondNumber) {
	return firstNumber / secondNumber;
	console.log("This message will not be printed.")
}

let divideNumber = returnQuotient(genFirstNumber, genSecondNumber);
console.log("The quotient of " + genFirstNumber + " and " + genSecondNumber);
console.log(divideNumber); 

/*	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.*/

function returnAreaOfCircle(radiusCircle) {
	let circleArea = piOfCircle* (radiusCircle ** 2);
	return circleArea
	console.log("This message will not be printed.")
}

let areaCircle = returnAreaOfCircle(radiusCircle);
console.log("The result of getting the area of a circle with " + radiusCircle + " radius: ");
console.log(areaCircle); 

/*	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/

function returnAverageFour(firstAve, secondAve, thirdAve, fourthAve) {
	let totalAverage = (firstAve + secondAve + thirdAve + fourthAve) / 4;
	return totalAverage;
	console.log("This message will not be printed.")
}

let averageOfFour = returnAverageFour(firstAve, secondAve, thirdAve, fourthAve);
console.log("The average of " + firstAve + ", " + secondAve + ", " + thirdAve + ", " + fourthAve + ":");
console.log(averageOfFour); 	

/*	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

function isPassing(myScore, totalScore) {
	let myPercentage = myScore / totalScore;
	let isPassingScore = myPercentage >= passingGrade;
	return isPassingScore
	console.log("This message will not be printed.")
}

let myGrade = isPassing(myScore, totalScore);
console.log("Is " + myScore + "/" + totalScore + " a passing score?");
console.log(myGrade); 