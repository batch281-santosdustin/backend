// Function with Parameters

function printName(name) {
	console.log("My name is " + name);
}

printName("Juana");
printName("John");

let sampleVariable = "Bella";
printName(sampleVariable);

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("Remainder of " + num + " divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?")
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);

// Functions as arguments
function argumentFunction() {
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction) {
	argumentFunction();
}

invokeFunction(argumentFunction);

// Multiple parameters
function createFullName (firstName, middleName, lastName) {
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

createFullName('Juan', 'Perez', 'dela Cruz');

// Using variables as arguments
// let firstName = prompt("First name: ");
// let middleName = prompt("Middle name: ");
// let lastName = prompt("Last name: ");

let firstName = "John"
let middleName = "Doe"
let lastName = "Smith"
createFullName(firstName, middleName, lastName);

// Return Statement
function returnFullName(firstName, middleName, lastName) {
	return firstName + " " + middleName + " " + lastName;
	console.log("This message will not be printed.")
}

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);
console.log(returnFullName(firstName, middleName, lastName));

function returnAddress(city, country) {
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);