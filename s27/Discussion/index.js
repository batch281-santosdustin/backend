// MongoDB - Data Modeling and Translation

// What is a data model?
/*
	Situation 1: Web app will start with a simple login procedure.
		There will be two types of users: admin and costumer
		Information about the admin will be placed in the db beforehand.
		For the customers, they need to register before logging in.
		During registration, name and contact details of the customer.
		Admin is responsible for adding the details ofthe course they offer. The admin should also be able to update whether the course is being offered or not
		Customers shall have information on which course they enrolled to. At the same time, course shall also contain a list of customers taht enrolled to a given course.
		Finally, customers will enroll to a course and will pay for the courses they've enrolled to create a transaction. The transaction will record the status of the payment and the payment method that the customer used.

*/

// Creating the data model
/*
	Three major models:
		1. Users: admin and customer
			First name
			Last name
			Email
			Password
			Is admin?
			Mobile number
			Enrollments
		2. Courses
			Name
			Description
			Price
			Is active?
			Slots
			Enrollees
		3. Transaction
			USER ID
			Course ID
			Is paid?
			Payment method
*/

// Translating Data models into an ERD
/*
	ERD - entity relationship diagram. It is a diagram to describe the structure of the database design.

	Commonly used for SQL and relational databases

	Since NoSQL is a non-relational db meaning there no joining tables that connect multiple tables together, they would normally have arrays with a list of unique ids of other objects that connect them.

	ERDs are still commonly used by developers to create a visual representation of how data is structured.

	The following are symbols that are currently used in ERD diagrams:
		*** https://www.lucidchart.com/pages/ER-diagram-symbols-and-meaning ***
*/

// Identifying relationships between data models
/*
	1. One-to-one
	2. One-to-many
	3. Many-to-many
*/

// Drawing an ERD using diagrams.net
