// Import the http module using the required directive
const http = require('http');

// Create a variable port and assign it with the value of 3000.
const port = 3000;

// Create a server using the createServer method that will listen in to the port provided above.
const app = http.createServer((request, response) => {
	// Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Welcome to the login page');
	}
	else {
		response.writeHead(404, {'Content-Tpe' : 'text/plain'});
		response.end("I'm sorry, the page you are looking for cannot be found.");
	}
})

app.listen(port);

//  Console log in the terminal a message when the server is successfully running.
console.log(`Server now accessible at localhost: ${port}.`);