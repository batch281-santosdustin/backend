let http = require("http");
/* http - used to transfer data from one to another */
/* require directive - to load the http module*/

// Creating a simple server
http.createServer(function (request, response) {
	// Returning what type of response being thrown to the client
	// 200 is status
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// Send the response with the text content, 'Hello, world'
	response.end('Hello, world!');
}).listen(4000);

// When server is running, console will print the message
console.log('Server is running at localhost:4000');