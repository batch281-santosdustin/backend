const http = require('http');

// Create a variable "port" to store the port number
const port = 4000;
const app = http.createServer((request, response) => {
	// Accessing the "greeting" route returns a message of "Hello, world!"
	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Hello again');
	}

	else if (request.url == '/homepage'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('This is the homepage');
	}

	else {
		response.writeHead(404, {'Content-Tpe' : 'text/plain'});
		response.end('Page not available');
	}
});

app.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);

// To run on browser: localhost:4000/greeting